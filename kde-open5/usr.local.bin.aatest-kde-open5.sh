# vim:syntax=apparmor

#include <tunables/global>

# TODO: why this does not work???
#@{aatest-kde-open5_executable}=/usr/local/bin/aatest-kde-open5.sh

# TODO: why this does not work???
#profile aatest-kde-open5 @{aatest-kde-open5_executable} {
profile aatest-kde-open5 /usr/local/bin/aatest-kde-open5.sh {
  #include <abstractions/base>
  #include <abstractions/bash>
  #include <abstractions/ubuntu-helpers>

  # Main executables

  /{,usr/}bin/bash mrix,
  #@{aatest-kde-open5_executable} r,
  /usr/local/bin/aatest-kde-open5.sh r,

  # Additional executables

  /usr/bin/env ix,
  /usr/bin/kde-open5 rPx -> aatest-kde-open5//kde-open5,

  # System files

  /dev/tty rw,

}
  # Child profiles

profile aatest-kde-open5//kde-open5 {
  #include <abstractions/kde-open5>
  #include <abstractions/ubuntu-helpers>
  #include <abstractions/ubuntu-browsers>

  # For alert sound support in message box.
  # Abstraction is not always available.
  #include if exists <abstractions/gstreamer>

  # a11y not enabled by default
  #include <abstractions/dbus-accessibility>
}
