Manual test for kde-open5 abstraction.

* Copy `aatest-kde-open5 into `/usr/local/bin/`
* Symlink `usr.local.bin.aatest-kde-open5` into `/etc/apparmor.d/`
* `sudo aa-enforce /etc/apparmor.d/usr.local.bin.aatest-kde-open5`
* Run `aatest-kde-open5.sh`, check for DENIED messages in kernel/audit log.

