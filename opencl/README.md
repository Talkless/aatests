Manual test for opencl abstraction.

* Copy `aatest-opencl` into `/usr/local/bin/`
* Symlink `usr.local.bin.aatest-opencl` into `/etc/apparmor.d/`
* `sudo aa-enforce /etc/apparmor.d/usr.local.bin.aatest-opencl`
* Run `aatest-opencl.sh`, check for DENIED messages in kernel/audit log.

NOTE: it is recommented to clear caches before testing like this:
```
rm -rf ~/.cache/pyopencl/ ~/.cache/pytools/ ~/.cache/mesa_shader_cache/ ~/.cache/pocl/ ~/.nv/ComputeCache/
```

### Notes for Debian/Ubuntu

Required packages:

* `python-pyopencl python-pyopencl-doc`

### Notes for openSUSE

Required packages:

* `python-pyopencl` from [home:syeg repository](https://build.opensuse.org/package/show/home%3Asyeg/python-pyopencl).

* `python-cffi python-setuptools Mesa-libOpenCL libOpenCL1 opencl-headers clang5`
