#!/usr/bin/env bash

set -u

declare -a ALNTERNATIVES
ALTERNATIVES[0]=/usr/share/doc/python-pyopencl-doc/examples
ALTERNATIVES[1]=/usr/share/doc/python-pyopencl/examples
ALTERNATIVES[2]=/usr/share/doc/packages/python-pyopencl/examples

PYTHON_PYOPENCL_EXAMPLE_DIR=

for ALT in ${ALTERNATIVES[@]}
do
    if [[ -e "${ALT}" ]]; then PYTHON_PYOPENCL_EXAMPLE_DIR="${ALT}"; break; fi
done

if [[ -z "${PYTHON_PYOPENCL_EXAMPLE_DIR}" ]]
then
    >&2 echo "Unsuported Linux disribution or missing python-opencl"
    exit 1
fi

python2 "${PYTHON_PYOPENCL_EXAMPLE_DIR}/demo.py"
