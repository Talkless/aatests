# vim:syntax=apparmor

#include <tunables/global>

profile aatest-opencl /usr/local/bin/aatest-opencl.sh {
  #include <abstractions/base>
  #include <abstractions/bash>
  #include <abstractions/opencl>
  #include <abstractions/python>

  # Main executables

  /usr/local/bin/aatest-opencl.sh r,
  /{,usr/}bin/{d,b}ash mrix,

  # Additional executables

  /usr/bin/env ix,
  /usr/bin/python{,2.7} ix,
  /{,usr/}sbin/ldconfig PUx,
  /{,usr/}bin/{,f}grep rix,

  # System files

  /dev/pts/[0-9] rw,
  /dev/tty rw,
  /etc/os-release r,
  /usr/share/doc/python-pyopencl{,-doc}/examples/{,**} r,
  /usr/share/doc/packages/python-pyopencl/examples/{,**} r, # for openSUSE
  /sys/devices/system/node/node[0-9]*/cpumap r, # for openSUSE libopenblas_pthreads.so (python-pyopencl specific?)

  # User files

  owner /tmp/* rw,
  owner @{HOME}/.cache/pyopencl/ w,
  owner @{HOME}/.cache/pyopencl/** rw,
  owner @{HOME}/.cache/pyopencl/**/lock rwk,
  owner @{HOME}/.cache/pyopencl/**/lock rwk,
  owner @{HOME}/.cache/pytools/ w,
  owner @{HOME}/.cache/pytools/** rw,
}
