#!/usr/bin/env bash

set -e
set -u

function _launch {
    xdg-open $1
    read -p "Check if '${1}' is opened successfully. Press ENTER to continue."
}

_launch http://duckduckgo.com
xdg-open foo://bar
read -p "Check if error message box is shown. Press ENTER to continue."

