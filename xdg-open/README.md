Manual test for xdg-open abstraction.

* Copy `aatest-xdg-open` into `/usr/local/bin/`
* Symlink `usr.local.bin.aatest-xdg-open` into `/etc/apparmor.d/`
* `sudo aa-enforce /etc/apparmor.d/usr.local.bin.aatest-xdg-open`
* Run `aatest-xdg-open.sh`, check for DENIED messages in kernel/audit log.

