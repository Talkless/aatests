# vim:syntax=apparmor

#include <tunables/global>

# TODO: why this does not work???
#@{aatest-xdg-open_executable}=/usr/local/bin/aatest-xdg-open.sh

# TODO: why this does not work???
#profile aatest-xdg-open @{aatest-xdg-open_executable} {
profile aatest-xdg-open /usr/local/bin/aatest-xdg-open.sh {
  #include <abstractions/base>
  #include <abstractions/bash>

  # Main executables

  /{,usr/}bin/bash mrix,
  # TODO: why this does not work???
  #@{aatest-xdg-open_executable} r,
  /usr/local/bin/aatest-xdg-open.sh r,

  # Additional executables

  /usr/bin/env ix,
  /usr/bin/xdg-open rPx -> aatest-xdg-open//xdg-open,

  # System files

  /dev/tty rw,

}

# Child profiles

profile aatest-xdg-open//xdg-open {
  #include <abstractions/xdg-open>
  #include <abstractions/ubuntu-helpers>
  #include <abstractions/ubuntu-browsers>
}
