#!/usr/bin/env bash

set -e
set -u

function _launch {
    gvfs-open "${1}"
    read -p "Check if '${1}' is opened successfully. Press ENTER to continue."
}

_launch http://duckduckgo.com

