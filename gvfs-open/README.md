Manual test for gvfs-open abstraction.

* Copy `aatest-gvfs-open.sh` into`/usr/local/bin/`
* Symlink `usr.local.bin.aatest-gvfs-open` into `/etc/apparmor.d/`
* `sudo aa-enforce /etc/apparmor.d/usr.local.bin.aatest-gvfs-open`
* Run `aatest-gvfs-open.sh`, check for DENIED messages in kernel/audit log.

