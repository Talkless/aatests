# vim:syntax=apparmor

#include <tunables/global>

# TODO: why this does not work???
#@{aatest-gvfs-open_executable}=/usr/local/bin/aatest-gvfs-open.sh

# TODO: why this does not work???
#profile aatest-gvfs-open @{aatest-gvfs-open_executable} {
profile aatest-gvfs-open /usr/local/bin/aatest-gvfs-open.sh {
  #include <abstractions/base>
  #include <abstractions/bash>
  #include <abstractions/ubuntu-helpers>

  # Main executables

  /{,usr/}bin/bash mrix,
  #@{aatest-gvfs-open_executable} r,
  /usr/local/bin/aatest-gvfs-open.sh r,

  # Additional executables

  /usr/bin/env ix,
  /usr/bin/gvfs-open rPx -> aatest-gvfs-open//gvfs_open,

  # System files

  /dev/tty rw,

}

  # Child profiles

profile aatest-gvfs-open//gvfs_open {
  #include <abstractions/gvfs-open>
  #include <abstractions/ubuntu-helpers>
  #include <abstractions/ubuntu-browsers>
}

