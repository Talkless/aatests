Manual test for exo-open abstraction.

* Copy `aatest-exo-open` into `/usr/local/bin/`
* Symlink `usr.local.bin.aatest-exo-open` into `/etc/apparmor.d/`
* `sudo aa-enforce /etc/apparmor.d/usr.local.bin.aatest-exo-open`
* Run `aatest-exo-open.sh`, check for DENIED messages in kernel/audit log.

