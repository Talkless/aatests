# vim:syntax=apparmor

#include <tunables/global>

# TODO: why this does not work???
#@{aatest-exo-open_executable}=/usr/local/bin/aatest-exo-open.sh

# TODO: why this does not work???
#profile aatest-exo-open @{aatest-exo-open_executable} {
profile aatest-exo-open /usr/local/bin/aatest-exo-open.sh {
  #include <abstractions/base>
  #include <abstractions/bash>
  #include <abstractions/ubuntu-helpers>

  # Main executables

  /{,usr/}bin/bash mrix,
  #@{aatest-exo-open_executable} r,
  /usr/local/bin/aatest-exo-open.sh r,

  # Additional executables

  /usr/bin/env ix,
  /usr/bin/exo-open rPx -> aatest-exo-open//exo-open,

  # System files

  /dev/tty rw,

}

  # Child profiles

profile aatest-exo-open//exo-open {
  #include <abstractions/exo-open>
  #include <abstractions/ubuntu-helpers>
  #include <abstractions/ubuntu-browsers>
}
