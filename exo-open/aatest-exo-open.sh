#!/usr/bin/env bash

set -u

function _launch {
    exo-open $1
    read -p "Check if '${1}' is opened successfully. Press ENTER to continue."
}

_launch http://duckduckgo.com
exo-open foo://bar
read -p "Check if error message box is shown. Press ENTER to continue."

