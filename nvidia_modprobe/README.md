Manual test for nvida_modprobe named profile.

* Copy `aatest-nvidia_modprobe.sh` into `/usr/local/bin/`
* Symlink `usr.local.bin.aatest-nvidia_modprobe` into `/etc/apparmor.d/`
* `sudo aa-enforce /etc/apparmor.d/usr.local.bin.aatest-nvidia_modprobe`
* Run `aatest-nvidia_modprobe.sh`, check for DENIED messages in kernel/audit log.

