# vim:syntax=apparmor

#include <tunables/global>

profile aatest-nvidia_modprobe /usr/local/bin/aatest-nvidia_modprobe.sh {
  #include <abstractions/base>
  #include <abstractions/bash>

  # Main executables

  /usr/local/bin/aatest-nvidia_modprobe.sh r,
  /{,usr/}bin/{d,b}ash mrix,

  # Additional executables

  /usr/bin/env ix,
  /usr/bin/nvidia-modprobe Px -> nvidia_modprobe,

  # System files

  /dev/tty rw, # for bash

  # User files

  owner /dev/pts/[0-9]* rw, # for bash
}
