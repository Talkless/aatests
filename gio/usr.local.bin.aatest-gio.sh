# vim:syntax=apparmor

#include <tunables/global>

# TODO: why this does not work???
#@{aatest-gio_executable}=/usr/local/bin/aatest-gio.sh

# TODO: why this does not work???
#profile aatest-gio @{aatest-gio_executable} {
profile aatest-gio /usr/local/bin/aatest-gio.sh {
  #include <abstractions/base>
  #include <abstractions/bash>
  #include <abstractions/ubuntu-helpers>

  # Main executables

  /{,usr/}bin/bash mrix,
  #@{aatest-gio_executable} r,
  /usr/local/bin/aatest-gio.sh r,

  # Additional executables

  /usr/bin/env ix,
  /usr/bin/gio rPx -> aatest-gio//gio,

  # System files

  /dev/tty rw,

}
  
  # Child profiles

profile aatest-gio//gio {
  #include <abstractions/gio-open>
  #include <abstractions/ubuntu-helpers>
  #include <abstractions/ubuntu-browsers>
}
