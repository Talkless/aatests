Manual test for gio abstraction.

* Copy `aatest-gio`into` `/usr/local/bin/`
* Symlink `usr.local.bin.aatest-gio` into `/etc/apparmor.d/`
* `sudo aa-enforce /etc/apparmor.d/usr.local.bin.aatest-gio`
* Run `aatest-gio.sh`, check for DENIED messages in kernel/audit log.

