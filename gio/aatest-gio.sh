#!/usr/bin/env bash

set -u

function _launch {
    gio open "${1}"
    read -p "Check if '${1}' is opened successfully. Press ENTER to continue."
}

_launch http://duckduckgo.com
gio open foo://bar
read -p "Check if error is printed. Press ENTER to continue."

