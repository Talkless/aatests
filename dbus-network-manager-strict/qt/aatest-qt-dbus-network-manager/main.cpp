#include <QCoreApplication>
#include <QDebug>
#include <QNetworkConfiguration>
#include <QNetworkConfigurationManager>

void dumpAllNetworkConfigurations()
{
    const QNetworkConfigurationManager manager;
    const auto configurations = manager.allConfigurations();
    for (const QNetworkConfiguration &conf: configurations) {
        qDebug() << conf.name();
    }
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    dumpAllNetworkConfigurations();
    return 0;
}
