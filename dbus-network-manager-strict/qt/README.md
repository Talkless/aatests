Manual tests for dbus-network-manager-strict abstraction.

* Run install.sh to deploy manual test application and it's AppArmor profile.
* Run aatest-qt-dbus-network-manager, check for DENIED messages in kernel/audit log.

