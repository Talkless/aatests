#!/usr/bin/env bash

set -e -u pipefail

readonly SCRIPT_DIR=$(dirname $(readlink -f $0))
readonly BUILD_DIR=${SCRIPT_DIR}/_build

if [[ -z $(which apparmor_parser) ]]
then
   >&2 echo "AppArmor not found."
   exit 1
fi

echo "Installing build dependncies..."

sudo apt install build-essential qt5-default cmake
[[ -d  "${BUILD_DIR}" ]] && rm -rfv "${BUILD_DIR}"
mkdir -v "${BUILD_DIR}"

(
    pushd "${BUILD_DIR}"
    cmake ../aatest-qt-dbus-network-manager -DCMAKE_BUILD_TYPE=Release
    cmake --build .
    sudo cmake --build . --target install
)

echo "Copying AppArmor files..."
sudo cp -v "${SCRIPT_DIR}/usr.local.bin.aatest-qt-dbus-network-manager" "/etc/apparmor.d/"

echo "Restarting AppArmor..."
sudo systemctl restart apparmor

echo "Done. You now can run 'aatest-qt-dbus-network-manager' to check for AppArmor denials"

